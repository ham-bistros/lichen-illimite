console.log("coucou c'est main.js");

////////////////////
//   FONCTIONS    //
////////////////////

function addDelay(section) {
  const baseDelay = 0.3;

  [...section.children].forEach((el, index) => {
    const newDelay = baseDelay * (index + 1);

    if (el.classList.contains("reponse")) {
      el.style.animation = `apparition 1.5s ease both`;
      el.style.animationDelay = `${newDelay}s`;

      [...el.children[0].children].forEach((li, j) => {
        // le 3 fait une petite pause entre les paragraphes de la question et les réponses, qui arrivent dans une 2e salve
        // le 1.5 fait apparaître les réponses plus distinctement les unes des autres

        li.style.animation = `apparition 1.5s ease both`;
        li.style.animationDelay = `${baseDelay * (index + 3 + j * 1.5)}s`;
      });
    } else if (el.classList.contains("profiles")) {
      el.style.animation = `apparition 1.5s ease both`;
      el.style.animationDelay = `${baseDelay}s`;

      [...el.children].forEach((li, j) => {
        // le 3 fait une petite pause entre les paragraphes de la question et les réponses, qui arrivent dans une 2e salve
        // le 1.5 fait apparaître les réponses plus distinctement les unes des autres

        if (li.tagName != "AUDIO") {
          li.style.animation = `apparition 1.5s ease both`;
          li.style.animationDelay = `${baseDelay * (1 + j * 1.5)}s`;

          if (li.tagName == "H1") {
            setTimeout(() => {
              li.style.animation = `respiration 10s ease-in-out infinite alternate both`;
            }, baseDelay * 2000);
          }
        }
      });
    } else if (el.classList.contains("show")) {
      [...el.children].forEach((details, j) => {
        details.style.animation = `apparition 1.5s ease both`;
        details.style.animationDelay = `${baseDelay * (index + 1 + j)}s`;
      });
    } else if (el.tagName == "H1") {
      el.style.animation = `apparition 1.5s ease both`;
      el.style.animationDelay = `${newDelay}s`;

      // réactiver l'animation de respiration après l'apparition
      // limite de 4 parce que sinon tout tourne en même temps jcrois bien
      setTimeout(() => {
        el.style.animation = `respiration 10s ease-in-out infinite alternate both`;
      }, 3000);
    } else if (el.id == "text-video") {
      console.log("Pas d'animation pour cet élément");
    } else {
      el.style.animation = `apparition 1.5s ease both`;
      el.style.animationDelay = `${newDelay}s`;
    }
  });
}

function scrollMaxY(el) {
  let scrollMaxY = el.scrollMaxY || el.scrollHeight - el.clientHeight;
  return scrollMaxY;
}

function debounce(fn, delay) {
  var t;
  return function () {
    clearTimeout(t);
    t = setTimeout(fn, delay);
  };
}

////////////////////
//  DÉCLARATIONS  //
////////////////////

////////// SECTIONS
// moyen de rendre la variable sections globale
top.sections = document.querySelectorAll("section:not(#about)");

////////// ABOUT
const aboutButton = document.querySelector("#about-p");
const about = document.getElementById("about");
const close = document.getElementById("close");
let audioEl = document.querySelector(".show > #bg-sound");

////////////////////
//  PARTIE SONS   //
////////////////////

window.onload = function () {
  try {
    audioEl = document.querySelector(".show > #bg-sound");
    ////////// SONS D'AMBIANCE
    if (!audioEl.paused) {
      console.log("playing");
      audioEl.muted = false;
    }
    audioEl.play();
  } catch (error) {
    console.log("Pas d'audio de fond sur cette page");
  }
};

// Censé faire du autoplay sur les téléphones aussi sans user interaction
// ALORS PEUT-ÊTRE (après un fking long chargement)
// MAIS trop relou de choper le flux pour le mettre sur pauseĺe reprendre pendant que les capsules vidéos jouent quel enfer
// let audioElSrc = document.querySelector(".show > #bg-sound source");
//
// window.addEventListener("load", () => {
//   audioElSrc = document.querySelector(".show > #bg-sound source").src;
//   // noinspection JSUnresolvedVariable
//   let audioCtx = new (window.AudioContext || window.webkitAudioContext)();
//   let xhr = new XMLHttpRequest();
//   xhr.open("GET", audioElSrc);
//   xhr.responseType = "arraybuffer";
//   xhr.addEventListener("load", () => {
//     let playsound = (audioBuffer) => {
//       let source = audioCtx.createBufferSource();
//       source.buffer = audioBuffer;
//       source.connect(audioCtx.destination);
//       source.loop = true;
//       source.start();
//     };
//     audioCtx.decodeAudioData(xhr.response).then(playsound);
//   });
//   xhr.send();
// });

////////////////////
//  PARTIE DELAY  //
////////////////////

const firstSection = document.querySelector("section:not(#about)");
addDelay(firstSection);

////////////////////
//  PARTIE ABOUT  //
////////////////////
aboutButton.addEventListener("click", () => about.classList.toggle("display"));
close.addEventListener("click", () => about.classList.toggle("display"));

////////////////////
//  PARTIE OPTI   //
////////////////////

// Technique de ABC Dinamo
let observer = new IntersectionObserver(function (entries, observer) {
  entries.forEach(function (entry) {
    // Pause/Play the animation
    if (entry.isIntersecting) entry.target.style.animationPlayState = "running";
    else entry.target.style.animationPlayState = "paused";
  });
});

let variableTexts = document.querySelectorAll("h1");
variableTexts.forEach(function (el) {
  observer.observe(el);
});

////////////////////
//  PARTIE LIENS  //
////////////////////
// Ajoute un target="_blank" sur tous les liens externes
Array.from(document.getElementsByTagName("a"))
  .filter((link) => link.hostname != window.location.hostname)
  .forEach((link) => (link.target = "_blank"));
