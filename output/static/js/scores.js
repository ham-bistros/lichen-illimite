// Ici mettre des sons + probabelement les résultats pour chaque profil
// console.log("coucou c'est result.js");

// Récupérer l'url actuelle puis le paramètre "result"
const url = new URL(window.location.href);

const profiles = ["GEL", "FOL", "CRU", "FRU"];
const scorePoints = document.querySelectorAll(".barre-pourcentage");

let highest = [];

profiles.forEach((key) => {
    const pourcentage = `${url.searchParams.get(key)}`;
    const el = document.querySelector(`#${key} article`);

    if (highest[1]) {
        if (parseInt(highest[1]) < pourcentage) {
            highest = [key, pourcentage];
        }
    } else {
        highest = [key, pourcentage];
    }

    el.children[1].setAttribute("data-width", `${pourcentage}%`);
    el.style.setProperty("--local-width", `${pourcentage}%`);
});

// Effet sur le pourcentage le plus haut
const gros = document.querySelector(`#${highest[0]} article h3`);
setTimeout(() => gros.style.animation =  `respiration 4s ease-in-out infinite alternate both`, 2000);

// Faire apparaître le texte quelques secondes après le chargement de la page
window.onload = () => {
    const textVideo = document.getElementById("text-video");
    setTimeout(() => textVideo.style.opacity = 1, 8000);
}
