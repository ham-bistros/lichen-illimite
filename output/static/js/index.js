// ici mettre la roulette de départ
// console.log("coucou c'est index.js");

///////////////////////
//  PARTIE ROULETTE  //
///////////////////////


const roulette = document.getElementById("roulette");
const lienQuizz = document.querySelector('a[href*="quizz"]');

lienQuizz.addEventListener("mouseover", () => {
  let i = 1; // On commence au 2e élément car le 1er est déjà visible au début

  const refresh = setInterval(() => {
    roulette.children[(i-1) % roulette.children.length].style.visibility = "hidden";
    roulette.children[i % roulette.children.length].style.visibility = "visible";
    i++;
  }, 200);

  lienQuizz.addEventListener("mouseout", () => clearInterval(refresh));
})
