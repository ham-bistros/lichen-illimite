// Ici le système de calcul des points et le décelenchement des vidéos et
// diverses animations grâce au pouvoir de javascript

// console.log("coucou c'est quizz.js");

////////////////////
//  DÉFINITIONS   //
////////////////////

function doubleDigit(number) {
    return (number < 10 ? "0" : "") + number;
}

function displayNextQuestion(id, videoId) {
    let visible = document.querySelector(".page.display");
    const next = document.getElementById(id);

    // Cache la question actuelle
    visible.classList.toggle("display");
    visible.style.opacity = "0";
    visible.style.visibility = "hidden";

    // Affiche la question suivante
    if (!videoId) {
        // afficher avec un pti délai
        setTimeout(() => {
            next.classList.toggle("display");
            addDelay(next);
        }, 100);
    }

    for (let i = 0; i <= compteurQuestion; i++) {
        if (i != 20) {
            toc.children[i].style.color = hoverColor;

            // plus les questions sont anciennes plus elles sont lichenifiées
            const newTHCK =
                parseInt(baseStyle[1]) + nextTHCK * (compteurQuestion - i);
            const newGrow =
                parseInt(baseStyle[3]) + nextGrow * (compteurQuestion - i);

            toc.children[
                i
            ].style.fontVariationSettings = `"THCK" ${newTHCK}, "grow" ${newGrow}`;
        }
    }

    const autreTHCK = parseInt(baseStyle[1]) + (nextTHCK / 2) * compteurQuestion;
    const autreGrow = parseInt(baseStyle[3]) + (nextTHCK / 2) * compteurQuestion;

    tocMobileSpan.style.fontVariationSettings = `"THCK" ${autreTHCK}, "grow" ${autreGrow}`;
    // console.log(`"THCK" ${autreTHCK}, "grow ${autreGrow}"`);

    if (compteurQuestion == 20) {
        setTimeout(() => {
            tocMobileSpan.style.opacity = 0;
            for (let el of document.querySelectorAll(".numero-question")) {
                el.style.opacity = 0;
            }
            toc.style.opacity = 0;
        }, 500);
    }
}

function displayVideo(vid) {
    console.log(`Là on affiche ${vid}`);
    vid.style.display = "block";

    setTimeout(() => {
        vid.style.opacity = 1;
        toc.style.opacity = 0;
    }, 50);

    audioEl.pause(); // idéalement un fade out du son (pas prio)
}

function hideVideo(vid) {
    console.log("Finito la video");
    vid.style.opacity = 0;
    toc.style.opacity = 0.6;

    setTimeout(() => {
        vid.style.display = "none";
    }, 1000);
    audioEl.play(); // idéalement un fade in du son

    const next = document.getElementById(ordreQuestions[compteurQuestion - 1]);
    next.classList.toggle("display");
    addDelay(next);
}

function getWinner(score) {
    // Passe le résultat à la page de résultats en créeant une requête dans le
    // href "Découvre les résultats"
    const lien = document.getElementById("final-question");
    const urlParams = `CRU=${score.percent("CRU")}&FRU=${score.percent(
        "FRU"
    )}&GEL=${score.percent("GEL")}&FOL=${score.percent("FOL")}`;
    lien.setAttribute("href", `../scores/index.html?${urlParams}`);

    const h2 = document.getElementById("final-wrapper");
    h2.style.display = "block";

    setTimeout(() => {
        h2.style.opacity = "1";
    }, 3000);
}

function playPauseMedia(media) {
    if (media.paused) {
        media.play();
        // console.log("PLAY");
    } else {
        media.pause();
        // console.log("PAUSE");
    }
}

function skipVideo() {
    player.pause();
    hideVideo(vid.parentElement);
    audioEl.play();
}

// DÉBUT DU SCRIPT (???)
const questions = document.querySelectorAll(".reponse"); // Logique n'est-ce pas ?
const reponses = document.querySelectorAll('.reponse ul a[role*="radio"]');
const jauge = document.querySelectorAll("#jauge p");
const trickOrdre = document.querySelectorAll("#ordre-questions p");
let ordreQuestions = [];
[...trickOrdre].forEach((el) => ordreQuestions.push(el.innerHTML));

// Config pour le toc
const toc = document.getElementById("toc");
const tocMobileSpan = document.querySelector("#toc-mobile p");
const hoverColor = window
    .getComputedStyle(document.body)
    .getPropertyValue("--hover");
const baseStyle = window
    .getComputedStyle(toc.children[0])
    .getPropertyValue("font-variation-settings")
    .split(" ");

const maxTHCK = 1500;
const nextTHCK = (maxTHCK - parseInt(baseStyle[1])) / questions.length;
const maxGrow = 1200;
const nextGrow = (maxGrow - parseInt(baseStyle[3])) / questions.length;

const vid = document.getElementById("vimeo-player");

let compteurQuestion = 1;
let listeReponses = [];

class Score {
    constructor() {
        this.GEL = 0;
        this.FRU = 0;
        this.CRU = 0;
        this.FOL = 0;
    }

    total() {
        return this.GEL + this.FRU + this.CRU + this.FOL;
    }

    percent(cat) {
        const rawPercent = (100 * this[cat]) / this.total();
        return rawPercent.toFixed(1);
    }
}

////////////////////
// PARTIE SCORING //
////////////////////

let score = new Score();
const compteDouble = [5, 7, 11, 15, 17, 20];

[...reponses].forEach(function(reponse) {
    reponse.addEventListener("click", function(ev) {
        // Compte la question actuelle ainsi que les réponses données à chaque
        // question

        // Permet d'éviter de sélectionner le <p> ajouté avec le markdown mais
        // plutôt de prendre le <a>
        let rep;

        if (ev.target.tagName == "P") {
            rep = ev.target.parentElement;
        } else {
            rep = ev.target;
        }

        let scoreLichen = rep.classList[1];

        // Fait monter le score
        if (scoreLichen) {
            if (compteDouble.includes(compteurQuestion)) {
                score[scoreLichen] += 2;
            } else {
                score[scoreLichen] += 1;
            }
        }

        const videoId = rep.getAttribute("data-videoId");

        if (videoId) {
            console.log("Il y a une video →", videoId);

            playVideo(videoId);
            displayVideo(vid.parentElement);
        } else {
            console.log("Pas de video en vue");
        }

        displayNextQuestion(ordreQuestions[compteurQuestion], videoId);

        if (compteurQuestion == questions.length) {
            // On affiche les résultats seulement à la dernière phase
            getWinner(score);
        }

        compteurQuestion += 1;
    });
});

////////////////////
// PARTIE VIDEOS  //
////////////////////

function playVideo(videoId) {
    player.loadVideo(videoId).then(function() {
        console.log(`La video ${videoId} a bien été chargée !`)
    }).catch(function(e) {
        console.log("ERR:", e);
    });

    player.ready().then(function() {
        console.log("Video is ready");
        setTimeout(() => {
            player.play();
        }, 500);
    });
}

vid.addEventListener("change", function(ev) {
    console.log("event", ev);
    playPauseMedia(audioEl);
});

let player = new Vimeo.Player(vid);

player.on("play", function() {
    console.log("Video is playing");
});

player.on("ended", function() {
    hideVideo(vid.parentElement);
});

////////////////////
//   PARTIE TOC   //
////////////////////

////////// SONS DE TRANSITION
const audioz = document.getElementsByClassName("switch-sound");
const links = document.getElementsByTagName("a");

[...links].forEach((el) => {
    el.addEventListener("click", function() {
        var randomIndex = Math.floor(Math.random() * audioz.length);
        var audioLink = audioz[randomIndex].href;
        // console.log(audioLink);

        let transition = new Audio(audioLink);
        transition.play();
    });
});
