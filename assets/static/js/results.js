// Ici mettre des sons + probabelement les résultats pour chaque profil
// console.log("coucou c'est result.js");

// Récupérer l'url actuelle puis le paramètre "result"
const url_string = window.location.href;
const url = new URL(url_string);
const winner = document.getElementById(url.searchParams.get("profil"));

// Affiche le bon paragraphe
document.getElementById("profils").prepend(winner);
winner.classList.add("show");

const profiles = {
  "CRU": "crustacé",
  "FOL": "foliacé",
  "GEL": "gélatineux",
  "FRU": "fruticuleux"
};

const summaries = document.getElementsByTagName("summary");

for (let summary of summaries) {
  summary.addEventListener("click", () => {
    const lol = document.querySelector("section");

    // Petit décalage pour être sûr que le detail est bien ouvert/fermé avant de faire le check
    setTimeout(() => checkScroll(lol), 50);
  });
}

function profileLinks() {
  let hiddenProfiles = document.querySelectorAll(".profiles:not(.show)");

  [...hiddenProfiles].forEach((profil, index) => {
    document.getElementById(`profil${index}`).innerHTML = profiles[profil.id];
  });
}

function displayProfile(elem) {
  const current = document.querySelector(".show");
  current.classList.toggle("show");

  const newId = elem.innerHTML.slice(0, 3).replace("é", "e").toUpperCase();
  document.getElementById(newId).classList.toggle("show");
  profileLinks();
}

profileLinks();
